const general = {
  DefendManeuver: "Defender & Manobrar",
  AdvanceAttack: "Avançar & Atacar",
  EvadeObserve: "Fugir & Observar",
  demeanors: "Comportamentos",
  stats: "Stats",
  creativity: "Criatividade",
  focus: "Foco",
  harmony: "Harmonia",
  passion: "Paixão",
  principles: "Principios",
  moves: "Movimentos",
  historyQuestions: "Questões de História",
  growthQuestion: "Pergunta de Crescimento",
  connections: "Conexões",
  momentOfBalance: "Momento de Equilibrio",
  fightingTechnique: "Tecnica de Luta",
  home: "Inicio",
  playbooks: "Cartilhas",
};

export { general };
