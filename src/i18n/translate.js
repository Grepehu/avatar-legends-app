import labelsEN from "./en/index.js";
import labelsPT from "./pt/index.js";

/**
 * @typedef {('en'|'pt')} Langs
 */

/** @type {Langs} */
let lang = "en";
let t = labelsEN;

/**
 * Function that returns the translation objects
 * @param {Langs} newLang
 * @returns
 */
function changeLang(newLang) {
  lang = newLang;
  switch (lang) {
    case "pt":
      t = labelsPT;
      break;
    case "en":
    default:
      t = labelsEN;
      break;
  }
  const langChangeEvent = new CustomEvent("langchange");
  document.dispatchEvent(langChangeEvent);
}

/**
 * Listens for the change in URL from the client-side navigation
 * @param {(e: Event) => void} callback
 * @returns {void}
 */
function listenLangChange(callback) {
  document.addEventListener("langchange", callback, false);
}

export { lang, t, changeLang, listenLangChange };
