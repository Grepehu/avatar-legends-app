import * as Types from "/utils/types.js"; // eslint-disable-line

/** @type {Object.<string,Types.Move>} */
const featureMoves = {
  theLodestar: {
    name: "The Lodestar",
    description:
      "There's only one person you often let past your emotional walls.\n\nName your lodestar (choose a PC to start): [[TEXT]]\n\nYou can shift your lodestar to someone new when they **guide and comfort** you and you open up to them, or when you **guide and comfort** them and they open up to you. If you do choose to shift your lodestar, clear a condition.\n\nWhen you **shut down someone vulnerable to harsh words or icy silence**, shift your balance toward Results and roll with Results. On a hit, they mark a condition and you may clear the same condition. On a 10+, they also cannot shift your balance or **call you out** for the rest of the scene. On a miss, they have exactly the right retort; mark a condition and they\nshift your balance. You cannot use this on your lodestar.\n\nWhen your lodestar **shifts your balance** or **calls you out**, you cannot resist it. Treat an NPC lodestar calling you out as if you rolled a 10+, and a PC lodestar calling you out as if they rolled a 10+.\n\nWhen you **consult your lodestar for advice on a problem** (or permission to use your preferred solution), roll with Restraint. On a 10+ take all three; on a 7-9 they choose two:\n\n- You see the wisdom of their advice. They shift your balance; follow their advice and they shift your balance again.\n- The conversation bolsters you. Clear a condition or 2-fatigue.\n- They feel at ease offering their opinion. They clear a condition or 2-fatigue.\n\nOn a miss, something about their advice infuriates you. Mark a condition or have the GM shift your balance twice",
  },
  legacyOfExcellence: {
    name: "Legacy of Excellence",
    description:
      "You have dedicated yourself to accomplishing great, exciting deeds and becoming worthy of the trust others place in you. Choose four drives to mark at the start of play. When you fulfill a marked drive, strike it out, and mark growth or clear a condition. When your four marked drives are all struck out, choose and mark four new drives. When all drives are struck out, change playbooks or accept a position of great responsibility and retire from a life of adventure.\n\n[[CHECKBOX]] successfully lead your companions in battle\n\n[[CHECKBOX]] give your affection to someone worthy\n\n[[CHECKBOX]] start a real fight with a dangerous master\n\n[[CHECKBOX]] do justice to a friend or mentor's guidance\n\n[[CHECKBOX]] take down a dangerous threat all on your own\n\n[[CHECKBOX]] openly outperform an authority figure\n\n[[CHECKBOX]] save a friend's life\n\n[[CHECKBOX]] get a fancy new outfit\n\n[[CHECKBOX]] earn the respect of an adult you admire\n\n[[CHECKBOX]] openly call out a friend's unworthy actions\n\n[[CHECKBOX]] form a strong relationship with a new master\n\n[[CHECKBOX]] stop a fight with calm words\n\n[[CHECKBOX]] sacrifice your pride or love for a greater good\n\n[[CHECKBOX]] defend an inhabited place from dire threats\n\n[[CHECKBOX]] stand up to someone who doesn't respect you\n\n[[CHECKBOX]] make a friend live up to a principle they have neglected\n\n[[CHECKBOX]] show mercy or forgiveness to a dangerous person\n\n[[CHECKBOX]] stand up to someone abusing their power\n\n[[CHECKBOX]] tame or befriend a dangerous beast or rare creature\n\n[[CHECKBOX]] pull off a ridiculous stunt",
  },
};

/** @type {Object.<string,Types.Move>} */
const regularMoves = {
  thisWasAVictory: {
    name: "This was a Victory",
    description:
      "When you reveal that you have sabotaged a building, device, or vehicle right as it becomes relevant, mark fatigue and roll with **PASSION**. On a hit, your work pays off, creating an opportunity for you and your allies at just the right time. On a 7-9, the opportunity is fleeting—act fast to stay ahead of the consequences. On a miss, your action was ill-judged and something or someone you care about is hurt as collateral damage.",
  },
  takesOneToKnowOne: {
    name: "Takes One to Know One",
    description:
      "When you verbally needle someone by finding the weaknesses in their armor, roll with **FOCUS**. On a hit, ask 1 question. On a 7-9, they ask 1 of you as well:\n\n- What is your principle?\n- What do you need to prove?\n- What could shake your certainty?\n- Whom do you care about more than you let on?\n\nAnyone who lies or stonewalls marks 2-fatigue. On a miss, your attack leaves you exposed; they may ask you any one question from the list, and you must answer honestly.",
  },
  noTimeForFeelings: {
    name: "No Time For Feelings",
    description:
      "When you have equal or fewer conditions marked than your highest principle, mark fatigue to push down your feelings for the rest of the scene and ignore condition penalties until the end of the scene. When you **resist an NPC shifting your balance**, mark a condition to roll with conditions marked (max +4). You cannot then choose to clear a condition by immediately proving them wrong.",
  },
  iDontHateYou: {
    name: "I Don't Hate You",
    description:
      "When you **guide and comfort** someone in an awkward, understated, or idiosyncratic fashion, roll with **PASSION** instead of **HARMONY** if you mark Insecure or Insecure is already marked.",
  },
  drivenByJustice: {
    name: "Driven by Justice",
    description: "Take +1 to **PASSION** (max +3).",
  },
  bestFriend: {
    name: "Best Friend",
    description:
      "Your best friend is small, fuzzy, and dependable. Unlike all your other relationships, this one is simple and true. You can understand and communicate with your small companion and — although they may give you a hard time now and again — they are always there when you need them most. Whenever your pal could help you push your luck, mark fatigue to roll with **CREATIVITY** instead of **PASSION**. If your pet ever gets hurt, mark a condition.",
  },
  heresThePlan: {
    name: "Here's The Plan",
    description:
      "When you commit to a plan you've proposed to the group, roll with **CREATIVITY**; take a -1 for each of your companions who isn't on board. On a 10+, hold 2. On a 7-9, hold 1. You can spend your hold 1-for-1 while the plan is being carried out to overcome or evade an obstacle, create an advantage, or neutralize a danger; if any of your companions abandon you while the plan is underway, you must mark a condition. On a miss, hold 1, but your plan goes awry when you encounter surprising opposition.",
  },
  notDoneYet: {
    name: "Not Done Yet!",
    description:
      "Once per session, when you are taken out, shift your balance towards center to stay up for one more combat exchange. After that exchange ends, you become helpless, unconscious, or otherwise incapable of continuing on, and are taken out as normal.",
  },
  youMissedSomething: {
    name: "You Missed Something",
    description:
      "When you evaluate a friendly NPC's plan to get something done, roll with **FOCUS**. On a hit, the GM tells you how you can drastically improve the chances of success; get it done, and they're sure to come through on top. On a 7-9, the problems inherent in the plan are fairly serious; the NPC will be resistant to making the necessary changes. On a miss, something about the plan throws you for a loop; the GM tells you what obvious danger the NPC is ignoring…or what they're hiding about their intent.",
  },
  straightShooter: {
    name: "Straight Shooter",
    description:
      "When you tell an NPC the blunt, honest truth about what you really think of them and their plans, roll with **FOCUS**. On a hit, they'll look upon your honesty favorably; they'll answer a non-compromising question honestly and grant you a simple favor. On a 7-9, they also give you an honest assessment of how they see you; mark a condition. On a miss, you're a bit too honest—they're either furious or genuinely hurt.",
  },
};

export { featureMoves, regularMoves };
