import { general } from "./general.js";
import { playbooks } from "./playbooks.js";

export default {
  general,
  playbooks,
};
