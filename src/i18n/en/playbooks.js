import * as Types from "/utils/types.js"; // eslint-disable-line
import { featureMoves, regularMoves } from "./moves.js";
import { fightingTechniques } from "./fightingTechniques.js";

/** @type {Types.Playbook[]} */
const playbooks = [
  {
    key: "adamant",
    title: "The Adamant",
    demeanors: ["Above-it-all", "Rebellious", "Perfectionist", "Flippant", "Chilly", "Standoffish"],
    initialStatsArray: [0, 1, -1, 1],
    blackPrinciple: "Restraint",
    whitePrinciple: "Results",
    featureMove: featureMoves.theLodestar,
    moves: [
      regularMoves.thisWasAVictory,
      regularMoves.takesOneToKnowOne,
      regularMoves.noTimeForFeelings,
      regularMoves.iDontHateYou,
      regularMoves.drivenByJustice,
    ],
    historyQuestions: [
      "What experience of being deceived or manipulated convinced you to steel yourself against being swayed by other people?",
      "Who was your first lodestar, and why were they an exception? Why aren't they your lodestar anymore?",
      "Who earned your grudging respect by teaching you pragmatism?",
      "What heirloom or piece of craftsmanship do you carry to remind you to stay true to yourself?",
      "Why are you committed to this group or purpose?",
    ],
    growthQuestion: "Did you seek support or guidance from others?",
    connectionsPrompts: [
      "[[TEXT]] takes issue with my methods—perhaps they have a point, but I certainly can't admit that to them!",
      "[[TEXT]] my lodestar; something about them makes them the one person I let my guard down around.",
    ],
    momentOfBalance:
      "You've held true to a core of conviction even while getting your hands dirty to do what you deemed necessary. But balance means appreciating that other people are just as complex as you are, not merely obstacles or pawns. Tell the GM how you solve an intractable problem or calm a terrible conflict by relating to dangerous people on a human level.",
    fightingTechnique: fightingTechniques.pinpointAim,
  },
  {
    key: "bold",
    title: "The Bold",
    demeanors: ["Impatient", "Enthusiastic", "Sensitive", "Talkative", "Affable", "Impetuous"],
    initialStatsArray: [1, 1, 0, -1],
    blackPrinciple: "Loyalty",
    whitePrinciple: "Confidence",
    featureMove: featureMoves.legacyOfExcellence,
    moves: [
      regularMoves.bestFriend,
      regularMoves.heresThePlan,
      regularMoves.notDoneYet,
      regularMoves.youMissedSomething,
      regularMoves.straightShooter,
    ],
    historyQuestions: [
      "Why do you feel the need to prove yourself so badly?",
      "Who epitomizes the kind of big, bold figure you hope to be?",
      "Whose approval do you think you will never attain?",
      "What token or symbol do you wear to prove you are serious?",
      "Why are you committed to this group or purpose?",
    ],
    growthQuestion:
      "Did you express vulnerability by admitting you were wrong or that you should have listened to someone you ignored?",
    connectionsPrompts: [
      "[[TEXT]] scoffs at me and my plans; one day I'll show them what I can do.",
      "[[TEXT]] has a pretty good head on their shoulders; they're a great sounding board for my ideas.",
    ],
    momentOfBalance:
      "The greatest heroes of your age may have overwhelming confidence, but balance isn't about pursuing greatness for the sake of greatness. You find a way to stand with your companions like no one else ever could. Tell the GM how you strike down an impossibly strong enemy or obstacle to protect your friends from harm as the best version of yourself.",
    fightingTechnique: fightingTechniques.tagTeam,
  },
];

export { playbooks };
