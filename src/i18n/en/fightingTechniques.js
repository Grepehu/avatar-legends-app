import * as Types from "/utils/types.js"; // eslint-disable-line

/** @type {Object.<string,Types.FightingTechnique>} */
const fightingTechniques = {
  pinpointAim: {
    title: "Pinpoint Aim",
    type: "DefendManeuver",
    description:
      "Take the time you need to line up a perfect shot; become Prepared. In the next exchange, if you **advance and attack**, roll with **FOCUS** or **PASSION**, your choice. If you use Strike, you do not have to mark fatigue to choose what you inflict.",
  },
  tagTeam: {
    title: "Tag Team",
    type: "DefendManeuver",
    description:
      "Work with an ally against the same foe; choose an engaged foe and an ally—double any fatigue, conditions, or balance shifts that ally inflicts upon that foe.",
  },
};

export { fightingTechniques };
