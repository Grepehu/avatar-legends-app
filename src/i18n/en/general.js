const general = {
  DefendManeuver: "Defend & Maneuver",
  AdvanceAttack: "Advance & Attack",
  EvadeObserve: "Evade & Observe",
  demeanors: "Demeanors",
  stats: "Stats",
  creativity: "Creativity",
  focus: "Focus",
  harmony: "Harmony",
  passion: "Passion",
  principles: "Principles",
  moves: "Moves",
  historyQuestions: "History Questions",
  growthQuestion: "Growth Question",
  connections: "Connections",
  momentOfBalance: "Moment of Balance",
  fightingTechnique: "Fighting Technique",
  home: "Home",
  playbooks: "Playbooks",
};

export { general };
