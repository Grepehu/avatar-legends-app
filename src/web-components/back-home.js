const css = String.raw;
const html = String.raw;

class BackHome extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    this.render();
  }

  render() {
    this.innerHTML = html`
      <style>
        ${css`
          .back-home {
            display: flex;
            justify-content: center;
            align-items: center;
            width: fit-content;

            svg {
              height: 1.2rem;
              width: 1.2rem;
            }
          }
        `}
      </style>
      <web-link class="btn back-home" to="home">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          stroke-width="1.5"
          stroke="currentColor"
        >
          <path
            stroke-linecap="round"
            stroke-linejoin="round"
            d="M9 15 3 9m0 0 6-6M3 9h12a6 6 0 0 1 0 12h-3"
          />
        </svg>
      </web-link>
    `;
  }
}

window.customElements.define("back-home", BackHome);
