import * as Types from "/utils/types.js"; // eslint-disable-line
import { ListPlaybooks } from "./list-playbooks.js"; // eslint-disable-line
import { parseRichText } from "/utils/utils.js";
import { t } from "/i18n/translate.js";

const css = String.raw;
const html = String.raw;

class PlaybookItem extends HTMLElement {
  /** @type {Types.Playbook} */
  playbook;

  constructor() {
    super();
  }

  connectedCallback() {
    /** @type {ListPlaybooks} */
    const list = this.closest("list-playbooks");
    this.playbook = list.selectedPlaybook;
    this.render();
  }

  render() {
    this.innerHTML = html`
      <style>
        ${css`
          .playbook-item {
            display: flex;
            gap: 1rem;
            border: 5px double #fff;
            justify-content: center;
            padding: 1rem;
            border-radius: 15px;
            flex-wrap: wrap;

            h1,
            h2 {
              width: 100%;
              text-align: center;
            }

            .limited-section {
              max-width: 300px;
              ul {
                display: flex;
                flex-wrap: wrap;
                gap: 1rem;
                li {
                  list-style: none;
                }
              }
            }
          }
        `}
      </style>
      <div class="playbook-item">
        <h1>${this.playbook.title}</h1>
        <section class="limited-section">
          <h2>${t.general.demeanors}</h2>
          <ul>
            ${this.playbook.demeanors
              .map((demean) => {
                return html`<li>${demean}</li>`;
              })
              .join("")}
          </ul>
        </section>
        <section class="limited-section">
          <h2>${t.general.stats}</h2>
          <p>${t.general.creativity}: ${this.playbook.initialStatsArray[0]}</p>
          <p>${t.general.focus}: ${this.playbook.initialStatsArray[1]}</p>
          <p>${t.general.harmony}: ${this.playbook.initialStatsArray[2]}</p>
          <p>${t.general.passion}: ${this.playbook.initialStatsArray[3]}</p>
        </section>
        <section>
          <h2>${t.general.principles}</h2>
          <div>
            <p>${this.playbook.blackPrinciple}</p>
            <p>${this.playbook.whitePrinciple}</p>
          </div>
        </section>
        <section>
          <h2>${t.general.moves}</h2>
          <div>
            <h3>${this.playbook.featureMove.name}</h3>
            <p>${parseRichText(this.playbook.featureMove.description)}</p>
          </div>
          <div>
            ${this.playbook.moves
              .map(
                (move) => html`
                  <h3>${move.name}</h3>
                  <p>${parseRichText(move.description)}</p>
                `,
              )
              .join("")}
          </div>
        </section>
        <section>
          <h2>${t.general.historyQuestions}</h2>
          <ul>
            ${this.playbook.historyQuestions
              .map((question) => html` <li>${question}</li> `)
              .join("")}
          </ul>
        </section>
        <section>
          <h2>${t.general.growthQuestion}</h2>
          <p>${this.playbook.growthQuestion}</p>
        </section>
        <section>
          <h2>${t.general.connections}</h2>
          <ul>
            ${this.playbook.connectionsPrompts
              .map((prompt) => html` <li>${parseRichText(prompt)}</li> `)
              .join("")}
          </ul>
        </section>
        <section>
          <h2>${t.general.momentOfBalance}</h2>
          <p>${this.playbook.momentOfBalance}</p>
        </section>
        <section>
          <h2>${t.general.fightingTechnique}</h2>
          <div>
            <h3>${this.playbook.fightingTechnique.title}</h3>
            <h4>${t.general[this.playbook.fightingTechnique.type]}</h4>
            <p>${parseRichText(this.playbook.fightingTechnique.description)}</p>
          </div>
        </section>
      </div>
    `;
  }
}

window.customElements.define("playbook-item", PlaybookItem);
