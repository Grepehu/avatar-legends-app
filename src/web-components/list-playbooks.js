import * as Types from "/utils/types.js"; // eslint-disable-line
import { t, listenLangChange } from "/i18n/translate.js";

const css = String.raw;
const html = String.raw;

class ListPlaybooks extends HTMLElement {
  /** @type {HTMLElement} */
  nav;
  /** @type {HTMLElement} */
  main;
  /** @type {Types.Playbook[]} */
  playbooks;
  /** @type {Types.Playbook} */
  selectedPlaybook;

  constructor() {
    super();
    this.playbooks = t.playbooks;
    this.selectedPlaybook = t.playbooks[0];
  }

  connectedCallback() {
    this.initialRender();
    listenLangChange(() => {
      this.playbooks = t.playbooks;
      this.selectedPlaybook = t.playbooks.find((pb) => pb.key === this.selectedPlaybook.key);
      this.render();
    });
  }

  initialRender() {
    this.innerHTML = html`
      <style>
        ${css`
          list-playbooks {
            display: flex;
            flex-direction: column;
            align-items: center;

            nav {
              display: flex;
              gap: 1rem;
              padding: 1rem;
              justify-content: center;
              flex-wrap: wrap;

              button {
                color: #fff;
                border: none;
                cursor: pointer;
                background: none;
                font-size: 1.5rem;

                &.active {
                  color: var(--color-details);
                }
              }
            }
          }
        `}
      </style>
      <back-home></back-home>
      <nav></nav>
      <main></main>
    `;

    this.nav = this.querySelector("nav");
    this.main = this.querySelector("main");
    this.render();
  }

  render() {
    this.nav.innerHTML = "";

    this.playbooks.forEach((pb) => {
      const isActive = this.selectedPlaybook.key === pb.key;

      const btn = document.createElement("button");
      btn.innerHTML = pb.title;
      isActive ? btn.classList.add("active") : null;
      btn.addEventListener("click", () => {
        this.selectedPlaybook = pb;
        this.render();
      });
      this.nav.appendChild(btn);
    });

    this.main.innerHTML = "<playbook-item></playbook-item>";
  }
}

window.customElements.define("list-playbooks", ListPlaybooks);

export { ListPlaybooks };
