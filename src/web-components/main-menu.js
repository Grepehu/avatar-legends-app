import { t, listenLangChange } from "/i18n/translate.js";

const css = String.raw;
const html = String.raw;

class MainMenu extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    this.render();
    listenLangChange(() => {
      this.render();
    });
  }

  render() {
    this.innerHTML = html`
      <style>
        ${css`
          main-menu {
            display: flex;
            width: 100%;
            flex-grow: 1;
            height: 100%;

            nav {
              flex-grow: 1;
              display: flex;
              flex-direction: column;
              gap: 1rem;
              width: fit-content;
              align-items: center;
              justify-content: center;

              .btn {
                width: 30%;

                @media (max-width: 768px) {
                  width: 90%;
                }
              }
            }
          }
        `}
      </style>
      <nav>
        <web-link class="btn" to="home">${t.general.home}</web-link>
        <web-link class="btn" to="playbooks">${t.general.playbooks}</web-link>
      </nav>
    `;
  }
}

window.customElements.define("main-menu", MainMenu);
