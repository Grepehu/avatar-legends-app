import { changeLang } from "/i18n/translate.js";
import * as LangTypes from "/i18n/translate.js"; // eslint-disable-line

class LangSwitch extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    /** @type {LangTypes.Langs[]} */
    const langs = ["en", "pt"];
    langs.forEach((lang) => {
      const btn = document.createElement("button");
      btn.innerHTML = lang;
      btn.addEventListener("click", () => {
        changeLang(lang);
      });
      this.append(btn);
    });
  }
}

window.customElements.define("lang-switch", LangSwitch);
