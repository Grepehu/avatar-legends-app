import { simpleMarkdown } from "/libs/md-parse.js";

/**
 * Turns Rich Text from descriptions into full readable HTML
 * @param {string} text
 */
function parseRichText(text) {
  let formatedText = text;
  formatedText = formatedText.replace(/\[\[TEXT\]\]/g, '<span class="text-space"></span>');
  formatedText = formatedText.replace(/\[\[CHECKBOX\]\]/g, '<input type="checkbox"></input>');
  formatedText = simpleMarkdown(formatedText);
  return formatedText;
}

export { parseRichText };
