/**
 * @typedef {Object} Move
 * @property {string} name
 * @property {string} description
 */

/**
 * @typedef {Object} FightingTechnique
 * @property {string} title
 * @property {('DefendManeuver'|'AdvanceAttack'|'EvadeObserve')} type
 * @property {string} description
 */

/**
 * @typedef {Object} Playbook
 * @property {string} key
 * @property {string} title
 * @property {string[]} demeanors
 * @property {number[]} initialStatsArray
 * @property {string} blackPrinciple
 * @property {string} whitePrinciple
 * @property {Move} featureMove
 * @property {Move[]} moves
 * @property {string[]} historyQuestions
 * @property {string} growthQuestion
 * @property {string[]} connectionsPrompts
 * @property {string} momentOfBalance
 * @property {FightingTechnique} fightingTechnique
 */

export {};
